from django.test import TestCase
from django.urls.base import reverse


class MainTest(TestCase):

    def test_home(self):
        response = self.client.get(reverse("web:home"))
        self.assertEqual(response.status_code, 200)
